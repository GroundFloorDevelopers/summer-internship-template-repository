from django.contrib import admin
from django.urls import path
from . import views
from django.conf.urls import url

urlpatterns = [
    path('', views.product,name='product'),
    url(r'^/(?P<value>[\w-]+)/$',views.product,name='item_id'),
]
