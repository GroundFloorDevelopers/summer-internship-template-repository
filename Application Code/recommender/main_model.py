#!/usr/bin/env python
# coding: utf-8

# In[1]:


from PIL import Image
import requests
from io import BytesIO
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import warnings
from bs4 import BeautifulSoup
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import nltk
import math
import time
import re
import os
import seaborn as sns
from collections import Counter
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.metrics import pairwise_distances
from matplotlib import gridspec
from scipy.sparse import hstack
import plotly
import plotly.figure_factory as ff
from plotly.graph_objs import Scatter, Layout


# In[2]:


data=pd.read_csv('haha1.csv')


# In[3]:


data.head()


# In[4]:


from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import nltk


# In[5]:


stop_words = set(stopwords.words('english'))
print ('list of stop words:', stop_words)


# In[6]:


from gensim.models import Word2Vec
from gensim.models import KeyedVectors
import pickle


# In[7]:


with open('word2vec_model', 'rb') as handle:
    model = pickle.load(handle)


# In[8]:


def nlp_preprocessing(total_text, index, column):
    if type(total_text) is not int:
        string = ""
        for words in total_text.split():
            # remove the special chars in review like '"#$@!%^&*()_+-~?>< etc.
            word = ("".join(e for e in words if e.isalnum()))
            # Conver all letters to lower-case
            word = word.lower()
            # stop-word removal
            if not word in stop_words:
                string += word + " "
        data[column][index] = string


# In[9]:


for index, row in data.iterrows():
    nlp_preprocessing(row['Title'], index, 'Title')
    nlp_preprocessing(row['Price'], index, 'Price')
    nlp_preprocessing(row['Processor'], index, 'Processor')
    nlp_preprocessing(row['OS'], index, 'OS')
    nlp_preprocessing(row['Display'], index, 'Display')
    nlp_preprocessing(row['Storage'], index, 'Storage')
    nlp_preprocessing(row['Memory'], index, 'Memory')
    nlp_preprocessing(row['Post_purchase'], index, 'Post_purchase')
    nlp_preprocessing(row['GPU'], index, 'GPU')
    nlp_preprocessing(row['Battery_life'], index, 'Battery_life')


# In[10]:


data.head()


# In[11]:


from sklearn.feature_extraction.text import CountVectorizer
title_vectorizer = CountVectorizer()
title_features   = title_vectorizer.fit_transform(data['Title'])

price_vectorizer = CountVectorizer()
price_features   = price_vectorizer.fit_transform(data['Price'])

processor_vectorizer = CountVectorizer()
processor_features   = processor_vectorizer.fit_transform(data['Processor'])

os_vectorizer = CountVectorizer()
os_features   = os_vectorizer.fit_transform(data['OS'])

display_vectorizer = CountVectorizer()
display_features   = display_vectorizer.fit_transform(data['Display'])

storage_vectorizer = CountVectorizer()
storage_features   = storage_vectorizer.fit_transform(data['Storage'])

memory_vectorizer = CountVectorizer()
memory_features = memory_vectorizer.fit_transform(data['Memory'])

postpurchase_vectorizer = CountVectorizer()
postpurchase_features = memory_vectorizer.fit_transform(data['Post_purchase'])


gpu_vectorizer = CountVectorizer()
gpu_features = gpu_vectorizer.fit_transform(data['GPU'])


battery_vectorizer = CountVectorizer()
battery_features = battery_vectorizer.fit_transform(data['Battery_life'])


# In[12]:


extra_features = hstack((price_features,processor_features,os_features,display_features,storage_features,memory_features,postpurchase_features,gpu_features,battery_features)).tocsr()


# In[13]:


vocab = model.keys()
# this function will add the vectors of each word and returns the avg vector of given sentance
def build_avg_vec(sentence, num_features, doc_id, m_name):
    # sentace: its title of the apparel
    # num_features: the lenght of word2vec vector, its values = 300
    # m_name: model information it will take two values
        # if  m_name == 'avg', we will append the model[i], w2v representation of word i
        # if m_name == 'weighted', we will multiply each w2v[word] with the idf(word)

    featureVec = np.zeros((num_features,), dtype="float32")
    # we will intialize a vector of size 300 with all zeros
    # we add each word2vec(wordi) to this fetureVec
    nwords = 0

    for word in sentence.split():
        nwords += 1
        if word in vocab:
            if m_name == 'weighted' and word in  idf_title_vectorizer.vocabulary_:
                featureVec = np.add(featureVec, idf_title_features[doc_id, idf_title_vectorizer.vocabulary_[word]] * model[word])
            elif m_name == 'avg':
                featureVec = np.add(featureVec, model[word])
    if(nwords>0):
        featureVec = np.divide(featureVec, nwords)
    # returns the avg vector of given sentance, its of shape (1, 300)
    return featureVec


# In[14]:


idf_title_vectorizer = CountVectorizer()
idf_title_features = idf_title_vectorizer.fit_transform(data['Title'])


# In[15]:


def n_containing(word):
    # return the number of documents which had the given word
    return sum(1 for blob in data['Title'] if word in blob.split())

def idf(word):
    # idf = log(#number of docs / #number of docs which had the given word)
    return math.log(data.shape[0] / (n_containing(word)))


# In[16]:


idf_title_features  = idf_title_features.astype(np.float)

for i in idf_title_vectorizer.vocabulary_.keys():
    # for every word in whole corpus we will find its idf value
    idf_val = idf(i)

    # to calculate idf_title_features we need to replace the count values with the idf values of the word
    # idf_title_features[:, idf_title_vectorizer.vocabulary_[i]].nonzero()[0] will return all documents in which the word i present
    for j in idf_title_features[:, idf_title_vectorizer.vocabulary_[i]].nonzero()[0]:

        # we replace the count values of word i in document j with  idf_value of word i
        # idf_title_features[doc_id, index_of_word_in_courpus] = idf value of word
        idf_title_features[j,idf_title_vectorizer.vocabulary_[i]] = idf_val


# In[17]:


doc_id = 0
w2v_title_weight = []
# for every title we build a weighted vector representation
for i in data['Title']:
    w2v_title_weight.append(build_avg_vec(i, 300, doc_id,'weighted'))
    doc_id += 1
# w2v_title = np.array(# number of doc in courpus * 300), each row corresponds to a doc
w2v_title_weight = np.array(w2v_title_weight)


# In[18]:


def idf_w2v_brand(doc_id, w1, w2, num_results):
    # doc_id: apparel's id in given corpus
    # w1: weight for  w2v features
    # w2: weight for brand and color features

    # pairwise_dist will store the distance from given input apparel to all remaining apparels
    # the metric we used here is cosine, the coside distance is mesured as K(X, Y) = <X, Y> / (||X||*||Y||)
    # http://scikit-learn.org/stable/modules/metrics.html#cosine-similarity
    idf_w2v_dist  = pairwise_distances(w2v_title_weight, w2v_title_weight[int(doc_id)].reshape(1,-1))
    ex_feat_dist = pairwise_distances(extra_features, extra_features[int(doc_id)])
    pairwise_dist   = (w1 * idf_w2v_dist +  w2 * ex_feat_dist)/float(w1 + w2)

    # np.argsort will return indices of 9 smallest distances
    indices = np.argsort(pairwise_dist.flatten())[0:num_results]
    #pdists will store the 9 smallest distances
    pdists  = np.sort(pairwise_dist.flatten())[0:num_results]

    #data frame indices of the 9 smallest distace's
    df_indices = list(data.index[indices])
    return df_indices



# In[20]:


data


# In[22]:


data1=pd.read_csv('haha1.csv')


# In[23]:


data1.head(70)


# In[24]:


def predict(doc_id):

    return idf_w2v_brand(int(doc_id-1), 5, 25, 5)


# In[25]:





# In[ ]:
