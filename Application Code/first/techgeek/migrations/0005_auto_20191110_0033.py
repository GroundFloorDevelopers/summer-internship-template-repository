# Generated by Django 2.2.7 on 2019-11-09 19:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('techgeek', '0004_auto_20191109_2304'),
    ]

    operations = [
        migrations.RenameField(
            model_name='laptop',
            old_name='Img_src',
            new_name='Img',
        ),
    ]
