from django.apps import AppConfig


class TechgeekConfig(AppConfig):
    name = 'techgeek'
