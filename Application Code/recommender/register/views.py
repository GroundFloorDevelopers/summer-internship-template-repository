from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.contrib.auth.models import User,auth
from django.contrib import messages


# Create your views here.
def register(request):
    if request.method == 'POST':
        Username=request.POST['Username']
        first_name=request.POST['first_name']
        last_name=request.POST['last_name']
        email=request.POST['email']
        password1=request.POST['password1']
        password2=request.POST['password2']
        if password1==password2:
            if User.objects.filter(username=Username).exists():
                messages.info(request,'Username Taken')
                return redirect('./register')

            elif User.objects.filter(email=email).exists():
                messages.info(request,'Email taken')
                return redirect('./register')
            else :
                user=User.objects.create_user(username=Username,first_name=first_name,last_name=last_name,password=password1,email=email)
                user.save();
                print('user created')
        else :
            messages.info(request,'Password not matching..')
            return redirect('./register')
        return render(request,"home1.html")

    else:
        return render(request,"home.html")



def haha(request):
    return render(request,"home1.html")
