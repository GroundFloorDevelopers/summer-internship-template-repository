from django.shortcuts import render
from .models import laptop


# Create your views here.
def index(request):
    dests = laptop.objects.all()
    dests=dests[:6]
    return render(request, 'index.html', {'dests': dests})
