from import_export import resources
from .models import laptop


class laptopResource(resources.ModelResource):
    class Meta:
        model = laptop
