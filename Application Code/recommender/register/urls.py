from django.contrib import admin
from django.urls import path
from . import views


urlpatterns = [
    path('', views.register,name='register'),
    path('/haha', views.haha,name='haha')
]
