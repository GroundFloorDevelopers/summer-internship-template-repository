from django.db import models

# Create your models here.
class laptop(models.Model):
    asin=models.IntegerField(default=0)
    Brand=models.TextField()
    Title=models.TextField()
    Price=models.TextField()
    Image_src=models.ImageField(upload_to='pics')
    Processor=models.TextField()
    OS=models.TextField()
    Display=models.TextField()
    Storage=models.TextField()
    Memory=models.TextField()
    Post_purchase=models.TextField()
    GPU=models.TextField()
    Battery_life=models.TextField()
    Rating=models.DecimalField(max_digits=5, decimal_places=3)
